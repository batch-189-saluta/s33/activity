// console.log('Hello World')

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
/*.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});*/

.then((data) => {
	console.log(data.map((eachToDo) => {
		return {'title' : eachToDo.title}
	}))
})

// ---------------------------------------------------

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))

// ---------------------------------------------------

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		title: "Created to do list item",
		completed: true
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// -----------------------------------------------------

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		title: "Updated To Do List Item",
		description: "to update my to do list with a different data structure",
		status: "pending",
		dateCompleted: "pending"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// -----------------------------------------------------
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		title: "Updated To Do List Item",
		description: "to update my to do list with a different data structure",
		status: "Completed",
		dateCompleted: new Date()
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// -----------------------------------------------------

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))


